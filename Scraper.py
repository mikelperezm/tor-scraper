__author__ = 'Mikel Pérez de Mendiola'

import requests
import random
from ConnectionManager import ConnectionManager

def Scraper():
    

    cm = ConnectionManager(10)

    url = "http://ip-api.com/json/"
    key = cm.request(url)
    print("TOR IP:")
    print("=======")
    print(key.data.decode('utf-8'))
    print()
    
    #yourquery = "Credit card" 
    yourquery = "bitcoin wallets"

    if " " in yourquery:
        yourquery = yourquery.replace(" ","+")

    #ahmia.fi onion site
    url = "http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/search/?q={}".format(yourquery)
    print(url)

    #possible user agents
    ua_list = ["Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.19577"
    ,"Mozilla/5.0 (X11) AppleWebKit/62.41 (KHTML, like Gecko) Edge/17.10859 Safari/452.6", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2656.18 Safari/537.36"
    ,"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36", "Mozilla/5.0 (Linux; U; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13","Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"
    ,"Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_8; zh-cn) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"]
    ua = random.choice(ua_list)
    headers = {'User-Agent': ua}


    #response = request.get(url, headers=headers)
    response = cm.request(url)

    content = response.data.decode('utf-8')
    #content = response.text

   
    def findlinks(content):
        #takes in content - webpage in string format - then searches it with regex
        import re
        import random
        
        regexquery = "\w+\.onion"
        #regexquery used for finding onion links
        mineddata = re.findall(regexquery, content)
        
        #filename = "stiosOnionCreditCards.txt"
        filename = "stiosOnionCreditWallets.txt"
        print("Saving to ... ", filename)
        mineddata = list(dict.fromkeys(mineddata))
        
        for k in mineddata:
            with open(filename,"a") as newfile:
                k  = k + "\n"
                newfile.write(k)
        print("Onions links written to text file : ", filename)


    if response.status == 200:
        print("response OK. \n")
        
        #find the links
        findlinks(content)