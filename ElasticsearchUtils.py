__author__ = 'Mikel Pérez de Mendiola'

from elasticsearch import Elasticsearch
from datetime import datetime

import logging


ec_index_name = 'borrar'

def connect_elasticsearch():
    _es = None
    _es = Elasticsearch([{'host': 'localhost', 'port': 9200}])
    if _es.ping():
        #print('Conectado a ElasticSearch!!')
        pass
    else:
        print('No se ha podido conectar a ElasticSearch!!')
    return _es

if __name__ == '__main__':
  logging.basicConfig(level=logging.ERROR)


def ec_save(es, site, mails, btcwallets, onions, webtext):
    created = False
    # index settings
    try:
        onionsite = {
            'site' : site,
            'mails' : mails,
            'btcwallets' : btcwallets,
            'onions' : onions,
            'timestamp': datetime.now(),
            'webtext' : webtext
        }
        res = es.index(index=ec_index_name, body=onionsite, id=site)
        print(res['result'])

        created = True
    except Exception as ex:
        print(str(ex))
    finally:
        return created

def ec_search(es, url):

    try:    
        
        query_body = {
            "query": {
                "simple_query_string": {
                "fields": ["site"],
                "query": url,
                "default_operator": "AND"
                }
            }
        }

        result = es.search(index=ec_index_name, body=query_body)

    except Exception as ex:
        print(str(ex))
    finally:
        return result



def ec_search_btc(es, btc):

    try:    
        
        query_body = {
                "query": {
                    "bool": {
                    "must": [],
                    "filter": [
                        {
                        "bool": {
                            "should": [
                            {
                                "match": {
                                "btcwallets": btc
                                }
                            }
                            ],
                            "minimum_should_match": 1
                        }
                        },
                        {
                        "range": {
                            "timestamp": {
                            "format": "strict_date_optional_time"
                            }
                        }
                        }
                    ],
                    "should": [],
                    "must_not": []
                    }
                }
            }
    

        result = es.search(index=ec_index_name, body=query_body)

    except Exception as ex:
        print(str(ex))
    finally:
        return result
