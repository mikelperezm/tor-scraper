__author__ = 'Mikel Pérez de Mendiola'

import requests
import random
import re
import concurrent.futures
import sys
import os
import argparse

from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
from ConnectionManager import ConnectionManager
from ElasticsearchUtils import *
from bs4 import BeautifulSoup
from termcolor import cprint



ua_list = ["Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 Edge/18.19577"
    ,"Mozilla/5.0 (X11) AppleWebKit/62.41 (KHTML, like Gecko) Edge/17.10859 Safari/452.6", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2656.18 Safari/537.36"
    ,"Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/44.0.2403.155 Safari/537.36", "Mozilla/5.0 (Linux; U; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13","Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"
    ,"Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10_5_8; zh-cn) AppleWebKit/533.20.25 (KHTML, like Gecko) Version/5.0.4 Safari/533.20.27"]
       

linksVisited = []
linksPending = []

onionsTemp = []

cm = ConnectionManager(50)
es = connect_elasticsearch()
es.indices.create(index=ec_index_name, ignore=400)


def addToPendingLinks(link):
    if (link not in linksVisited) and (link not in linksPending):
        result = ec_search(es, link)
        
        if result['hits']['total']['value'] == 0:
            linksPending.append(link)
      
def getURL(url):
    ua = random.choice(ua_list)
    headers = {'User-Agent': ua}
    print("Getting URL...", url)

    resp = cm.request(url)

    try:
        print("URL status: ",resp.status)
    except Exception as e:
        print("Exception: ",str(e))
        
    result = resp.data.decode('utf-8')
    
    return result

def extractLinksFromURL(url,resultf):
    urlToAdd = ""
    for href in resultf.find_all('a', href=True): 
        if ".com" not in href['href']:
            if ".onion" in href['href'] or "http:" in href['href'] or "https:" in href['href']:
                urlToAdd = href['href']
            else:
                urlToAdd = url+href['href']

            addToPendingLinks(urlToAdd)

def getElementsOfInterest(url,result, resultf):
    regex_onion = "\w+\.onion"
    onions = list(set(re.findall(regex_onion, result)))
    print("onions found: ",onions)

        
    #regex_bitcoin = "^(bc1|[13])[a-zA-HJ-NP-Z0-9]{25,39}$"
    regex_bitcoin1 = "bc1[ac-hj-np-z02-9]{8,87}"
    regex_bitcoin2 = "[13][a-km-zA-HJ-NP-Z1-9]{25,35}"

    btcwallets = list(set(re.findall(regex_bitcoin1, result)))
    btcwallets.append(list(set(re.findall(regex_bitcoin2, result))))


    print("bitcoin wallets found: ",btcwallets)

    regex_mail = "(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
    mails = list(set(re.findall(regex_mail, result)))
    print("mails found: ",mails)

    webtext = resultf.get_text()

    ec_save(es, url,mails,btcwallets,onions, webtext)
           
    return onions

def torSearcher(url):
    
    result = getURL(url)
    resultf = BeautifulSoup(result,features="lxml")

    extractLinksFromURL(url,resultf)
    onions = getElementsOfInterest(url,result, resultf)

    map(addToPendingLinks,onions)
    
    linksVisited.append(url)    
    
    while len(linksPending) > 0: 

        url = linksPending.pop(0)
        result = getURL(url)
        resultf = BeautifulSoup(result,features="lxml")

        getElementsOfInterest(url,result, resultf)

        linksVisited.append(url)      

    url = url.replace("http://","")

def searchbtc(btc):
    btc_results = ec_search_btc(es,btc)
    
    print("")
    print("="*69)
    print("Onions containing bitcoin address ",btc)
    print("="*69)
    print("")

    for site in btc_results['hits']['hits']:
        print(site['_id'])
    
def test():
    print("Test...")


try:
    
    cprint('##### TOR Crawler #####', "green")

    """ 
    programname = os.path.basename(sys.argv[0])
    thelist = sys.argv[1]

    if thelist == "crawl":
        
        thelist = sys.argv[2]
        print("Opening ...", thelist)
        with open(thelist, "r", encoding="utf-8") as newfile:
            data = newfile.readlines()
            
            data = list(map(lambda st: "http://" + str.replace(st, "\n", ""), data))

                
            with concurrent.futures.ProcessPoolExecutor(max_workers=1) as executor:
                future_results = executor.map(torSearcher, data)

    elif thelist == "test":
        test()
    elif thelist == "searchbtc":
        btc = sys.argv[2]
        searchbtc(btc)
    else:
        print("Choose a valid option")     """

    parser = argparse.ArgumentParser()
    parser.add_argument("--crawl", help="Start crawling")
    parser.add_argument("--searchbtc", help="Retrieve onion sites containing the bitcoin wallet address")

    args = parser.parse_args()


    if args.crawl:
        
        with open(args.crawl, "r", encoding="utf-8") as newfile:
                data = newfile.readlines()
                
                data = list(map(lambda st: "http://" + str.replace(st, "\n", ""), data))

                    
                with concurrent.futures.ProcessPoolExecutor(max_workers=3) as executor:
                    future_results = executor.map(torSearcher, data)
    elif args.searchbtc:
        searchbtc(args.searchbtc)
except Exception as e:
    print("Exception: ",e)
   


